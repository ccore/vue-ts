import { useStore } from "@/store"
import { Store } from "vuex"
export function usePermission(pageName: string, per: string, store?: Store<any>): boolean {
  if (!store) {
    store = useStore()
  }
  const perMap = store.state.login.permission
  let verifyPer: string = `system:${pageName}:${per}`
  // console.log(verifyPer);
  // console.log(typeof perMap.has(verifyPer));


  return perMap.has(verifyPer)
}
