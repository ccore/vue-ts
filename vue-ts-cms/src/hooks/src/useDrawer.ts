
import { ref } from "vue"
import PageDrawer from "@/components/page-drawer"
type callbackType = (item?: any) => void
export function useDrawer(callbackEdit?: callbackType, callbackCreated?: callbackType): any {
  const pageDrawerRef = ref<InstanceType<typeof PageDrawer>>()

  // 编辑或者新建
  const handleCreateOrEdit = (val: any) => {
    if (pageDrawerRef.value) {
      if (val) {
        callbackEdit && callbackEdit(val)
      } else {
        callbackCreated && callbackCreated()
      }
      pageDrawerRef.value.isShowDrawer = true
    }
  }

  return [handleCreateOrEdit, pageDrawerRef]
}
