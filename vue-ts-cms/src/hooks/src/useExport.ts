
import { ref } from "vue"

import ExportData from "@/components/Excel/exportData.vue"
import ImportData from "@/components/Excel/importData.vue"

export function useExport(pageName: string, store: any) {
  const exportDataRef = ref<InstanceType<typeof ExportData>>()
  const importDataRef = ref<InstanceType<typeof ImportData>>()
  const fistLetterUpperCase = pageName.slice(0, 1).toLocaleUpperCase() + pageName.slice(1) + 's'
  const dataList = store.state[`entire${fistLetterUpperCase}`]
  const handleExportData = () => {
    exportDataRef.value!.isShowDialog = true
    exportDataRef.value!.tableName = ''
  }

  const handleImportData = () => {
    importDataRef.value!.showExcel = true
  }
  const handleOnSuccess = (dataList: any[]) => {
    // 调用插入的接口
    store.dispatch(`${pageName}/${pageName}CreateBatchAction`, dataList)
    importDataRef.value!.showExcel = false

  }

  return {
    handleExportData,
    exportDataRef,
    importDataRef,
    dataList,
    handleImportData,
    handleOnSuccess
  }

}
