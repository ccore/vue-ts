
import { ref } from "vue"
import PageSearch from "@/components/page-search"
import PageTable from "@/components/page-table"
export function usePageSearch(): any {
  const myTableRef = ref<InstanceType<typeof PageTable>>()
  const mySearchRef = ref<InstanceType<typeof PageSearch>>()

  const handleSearch = (searchKeywords: any, associateModel: any) => {
    // 拼接where 语句
    if (searchKeywords['associateModal']) {
      searchKeywords.associateModal = {
        associateKey: associateModel.associateKey,
        associateVal: searchKeywords['associateModal']
      }
    }
    myTableRef.value?.getTableDataListDispatch(searchKeywords)
  }
  const handleReset = () => {
    myTableRef.value?.getTableDataListDispatch()
  }

  return [
    handleSearch,
    handleReset,
    myTableRef,
    mySearchRef
  ]


}
