import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

import { LStorage } from "@/utils/cacheInfo"
import { TOKEN } from "@/store/const"
// import { firstMenu } from "@/utils/menu-map"
import { ElMessage } from "element-plus"
import { start, done } from "@/utils/nprogress"
const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: '/main'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/login.vue')
  },
  {
    path: '/main',
    name: 'main',
    component: () => import('@/views/main.vue'),
    children: [
      {
        path: 'dashboard',
        name: 'dashboard',
        component: () => import('@/views/main/analysis/dashboard/dashboard.vue'),
        meta: {
          title: "核心技术"
        }
      },
      {
        path: 'overview',
        name: 'overview',
        component: () => import('@/views/main/analysis/overview/overview.vue'),
        meta: {
          title: "数据分析"
        }
      }
    ]

  },
  {
    path: '/401',
    name: 'notPermission',
    component: () => import('@/base-ui/not-permission/not-permission.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'notFound',
    component: () => import('@/base-ui/not-found/not-found.vue')
  }

]

let router = createRouter({
  routes,
  history: createWebHashHistory()
})


/**
 * 路由守卫
 *
 *
 */
// const flag = false
// const permission: string[] = []
const whitePath = ['/404', '/403']

router.beforeEach((to, from) => {
  start()
  // 设置浏览器标题
  if (to.meta.title) {
    document.title = String(to.meta.title);
  } else {
    document.title = 'vue3+Ts+RBAC权限管理';
  }
  // 跳转的不是登录页面
  const token = LStorage.get(TOKEN)
  if (to.path !== '/login') {
    // 没有携带 token
    if (!token) {
      router.push('/login')
    }
    // 跳转的是 '/main'
    if (to.path === '/main' || to.path === '/') {
      // 重定向到首页
      router.push('/main/dashboard')
    }
  }
})

router.afterEach(() => {
  done()
})




export default router
