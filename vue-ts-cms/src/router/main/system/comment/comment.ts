const comment = () => import('@/views/main/system/comment/comment.vue')
export default {
  path: '/main/system/comment',
  name: 'comment',
  component: comment,
  children: []
}
