interface ITodoList {
  title: string,
  desc: string
}

interface ILanguage {
  title: string,
  percentage: number,
  color?: string
}
interface IDisPlayData {
  title: string,
  icon: string,
  count: any
  color?: string
  otherOPtions?: any
}

export { ITodoList, ILanguage, IDisPlayData }
