import { IFormItem } from "@/base-ui/form";
export const baseSearchFormConfig = {
  labelWidth: '100px',
  labelPosition: 'left',
  itemStyle: { padding: '10px 40px' },
  colLayout: {
    xl: 6,
    lg: 8,
    md: 12,
    sm: 24,
    xs: 24,
  },
}

export const baseSearchFormItems: IFormItem[] = [
  {
    field: 'createdAt',
    label: "创建时间",
    type: "datepicker",
    otherOptions: {
      startPlaceholder: "开始时间",
      endPlaceholder: "结束时间",
      type: 'daterange'

    }
  },
]

export const baseTableConfig = {
  showIndex: true,
  showSelection: true,
  showPaging: true,
}


export const baseTablePropsList = [
  {
    prop: "createdAt",
    minWidth: "120",
    label: "创建时间",
    slotName: "createdAt",
    showOverflowTooltip: true
  },
  {
    label: "操作",
    minWidth: "140",
    slotName: "handle"
  }
]

