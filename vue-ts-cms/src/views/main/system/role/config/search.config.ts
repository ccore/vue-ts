import { IFormConfig } from "@/base-ui/form";
import { baseSearchFormConfig, baseSearchFormItems } from "../../base-config"
const searchFormConfig: IFormConfig = {
  ...baseSearchFormConfig,
  formItems: [
    {
      field: 'name',
      label: "角色名",
      type: "input",
      placeholder: "请输入角色名"
    },
    {
      field: 'identification',
      label: "角色标识",
      type: "input",
      placeholder: "请输入标识"
    },
    {
      field: 'status',
      label: "角色状态",
      type: "select",
      options: [
        {
          value: true,
          title: "启用",
        },
        {
          value: false,
          title: "禁用",
        }
      ]
    },
    {
      field: 'description',
      label: "角色描述",
      type: "input",
      placeholder: "请输入角色描述"
    },
    ...baseSearchFormItems
  ]
}
export { searchFormConfig }
