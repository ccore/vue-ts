export const ROLE_EXPORT = {
  角色名称: 'name',
  角色标识: 'identification',
  角色状态: 'status',
  角色等级: 'level',
  角色描述: 'description',
  创建时间: 'createdAt',
}
