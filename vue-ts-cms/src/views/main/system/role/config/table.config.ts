import { ITableConfig } from "@/base-ui/table/types.ts"
import { baseTableConfig, baseTablePropsList } from "../../base-config"
const tableConfig: ITableConfig = {
  title: "角色",
  ...baseTableConfig,
  propsList: [
    {
      prop: "name",
      minWidth: "60",
      label: "角色名称",
      slotName: "name",
      showOverflowTooltip: true

    },

    {
      prop: "identification",
      minWidth: "100",
      label: "角色标识",
      slotName: "identification"
    },
    {
      prop: "status",
      minWidth: "100",
      label: "状态",
      slotName: "status"

    },
    {
      prop: "description",
      minWidth: "160",
      label: "角色描述",
      slotName: "description",
      showOverflowTooltip: true
    },
    ...baseTablePropsList

  ]
}
export { tableConfig }
