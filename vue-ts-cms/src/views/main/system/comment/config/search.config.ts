import { IFormConfig } from "@/base-ui/form";
import { baseSearchFormConfig, baseSearchFormItems } from "../../base-config"
const searchFormConfig: IFormConfig = {
  ...baseSearchFormConfig,
  formItems: [
    {
      field: 'content',
      label: "评论内容",
      type: "input",
      placeholder: "请输入评论内容"
    },
    ...baseSearchFormItems

  ]
}
export { searchFormConfig }
