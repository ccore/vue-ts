import { IFormConfig } from "@/base-ui/form";

const modalFormConfig: IFormConfig = {
  labelWidth: '100px',
  labelPosition: 'left',
  itemStyle: {},
  colLayout: {
    span: 24
  },
  formItems: [
    {
      field: 'content',
      label: "评论内容",
      type: "textarea",
      placeholder: "请输分类名称"
    },

  ]
}
export { modalFormConfig }
