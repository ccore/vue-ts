import { IFormConfig } from "@/base-ui/form";

const modalFormConfig: IFormConfig = {
  labelWidth: '100px',
  labelPosition: 'left',
  itemStyle: {},
  colLayout: {
    span: 24
  },
  formItems: [
    {
      field: 'title',
      label: "文章标题",
      type: "input",
      placeholder: "请输文章标题"
    },
    {
      field: 'summary',
      label: "文章简介",
      type: "input",
      placeholder: "请输分文章简介"
    },

    {
      field: 'status',
      label: "状态",
      type: "radio",
      defaultValue: 1,
      options: [
        {
          value: 1,
          title: "启用",
        },
        {
          value: 0,
          title: "禁用",
        },

      ]
    },
    {
      field: 'associateIds',
      label: "分类",
      type: "checkbox",
      defaultValue: [2],
      isAssociate: true,
      associateName: 'categories',
      associateKey: 'id',
      options: [],
      otherOptions: {
        min: 1
      }
    },

    {
      field: 'content',
      label: "文章内容",
      type: "textarea",
      placeholder: "请输文章内容"
    },



  ]
}
export { modalFormConfig }
