import { IFormConfig } from "@/base-ui/form";
import { baseSearchFormConfig, baseSearchFormItems } from "../../base-config"
const searchFormConfig: IFormConfig = {
  ...baseSearchFormConfig,
  formItems: [
    {
      field: 'title',
      label: "文章标题",
      type: "input",
      placeholder: "请输文章标题"
    },
    {
      field: 'summary',
      label: "文章简介",
      type: "input",
      placeholder: "请输分文章简介"
    },
    {
      field: 'associateModal',
      isAssociate: true,
      associateKey: 'identification',
      label: "分类",
      type: "select",
      options: []
    },
    {
      field: 'status',
      label: "分类状态",
      type: "select",
      options: [
        {
          value: 1,
          title: "启用",
        },
        {
          value: 0,
          title: "禁用",
        }
      ]
    },
    ...baseSearchFormItems

  ]
}
export { searchFormConfig }
