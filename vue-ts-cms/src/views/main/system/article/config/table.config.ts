import { ITableConfig } from "@/base-ui/table/types.ts"
import { baseTableConfig, baseTablePropsList } from "../../base-config"
const tableConfig: ITableConfig = {
  title: "文章分类",
  ...baseTableConfig,
  propsList: [
    {
      prop: "title",
      minWidth: "160",
      label: "文章标题",
      slotName: "title",


    },
    {
      prop: "cover",
      minWidth: "100",
      label: "文章封面",
      slotName: "cover",
    },
    {
      prop: "summary",
      minWidth: "100",
      label: "文章简介",
      slotName: "summary",
      showOverflowTooltip: true
    },
    {
      prop: "view_count",
      minWidth: "100",
      label: "文章浏览量",
      slotName: "view_count"
    },

    {
      prop: "content",
      minWidth: "100",
      label: "文章内容",
      slotName: "content",
      showOverflowTooltip: true
    },
    {
      prop: "categories",
      minWidth: "100",
      label: "文章分类",
      slotName: "categories",
      showOverflowTooltip: true
    },

    {
      prop: "createdAt",
      minWidth: "120",
      label: "创建时间",
      slotName: "createdAt",
      showOverflowTooltip: true
    },
    {
      label: "操作",
      minWidth: "220",
      slotName: "handle"
    }
  ]
}
export { tableConfig }
