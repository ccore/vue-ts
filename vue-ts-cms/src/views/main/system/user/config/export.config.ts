export const USER_EXPORT = {
  用户名称: 'username',
  用户头像: 'avatar',
  用户状态: 'status',
  用户角色: 'userRoles',
  用户密码: 'password',
  联系方式: 'phone',
  创建时间: 'createdAt',
}
export const USER_IMPORT = {
  用户名称: 'username',
  用户头像: 'avatar',
  用户状态: 'status',
  用户密码: 'password',
  用户角色: 'associateIds',
  联系方式: 'phone',
  创建时间: 'createdAt',
}
