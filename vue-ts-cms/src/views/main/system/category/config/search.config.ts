import { IFormConfig } from "@/base-ui/form";
import { baseSearchFormConfig, baseSearchFormItems } from "../../base-config"
const searchFormConfig: IFormConfig = {
  ...baseSearchFormConfig,
  formItems: [
    {
      field: 'name',
      label: "分类名称",
      type: "input",
      placeholder: "请输分类名称"
    },
    {
      field: 'identification',
      label: "标识",
      type: "input",
      placeholder: "请输入分类标识"
    },
    {
      field: 'remark',
      label: "分类描述",
      type: "input",
      placeholder: "请输入分类描述"
    },

    {
      field: 'status',
      label: "分类状态",
      type: "select",
      options: [
        {
          value: 1,
          title: "启用",
        },
        {
          value: 0,
          title: "禁用",
        }
      ]
    },
    ...baseSearchFormItems

  ]
}
export { searchFormConfig }
