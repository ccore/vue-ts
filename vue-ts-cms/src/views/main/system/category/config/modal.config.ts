import { IFormConfig } from "@/base-ui/form";

const modalFormConfig: IFormConfig = {
  labelWidth: '100px',
  labelPosition: 'left',
  itemStyle: {},
  colLayout: {
    span: 24
  },
  formItems: [
    {
      field: 'name',
      label: "分类名称",
      type: "input",
      placeholder: "请输分类名称"
    },
    {
      field: 'identification',
      label: "分类标识",
      type: "input",
      placeholder: "请输分类标识"
    },

    {
      field: 'status',
      label: "状态",
      type: "radio",
      defaultValue: 1,
      options: [
        {
          value: 1,
          title: "启用",
        },
        {
          value: 0,
          title: "禁用",
        },

      ]
    },

    {
      field: 'remark',
      label: "分类描述",
      type: "textarea",
      placeholder: "请输分类描述"
    },



  ]
}
export { modalFormConfig }
