import { ITableConfig } from "@/base-ui/table/types.ts"
import { baseTablePropsList } from "../../base-config"
const tableConfig: ITableConfig = {
  title: "菜单",
  showIndex: false,
  showSelection: false,
  showPaging: false,
  childrenProps: {
    rowKey: 'id',
    treeProps: {
      children: 'children'
    }
  },
  propsList: [
    {
      prop: "menu_name",
      label: "菜单名称",
      minWidth: "120",
      slotName: "menu_name"

    },
    {
      prop: "path",
      label: "菜单路径",
      slotName: "path",
      showOverflowTooltip: true
    },

    {
      prop: "menu_type",
      label: "菜单类型",
      slotName: "menu_type"

    },
    {
      prop: "perms",
      label: "菜单权限",
      slotName: "perms",
      minWidth: "100",
      showOverflowTooltip: true
    },
    {
      prop: "status",
      minWidth: "100",
      label: "状态",
      slotName: "status"
    },
    {
      prop: "remark",
      label: "菜单描述",
      slotName: "remark",
      showOverflowTooltip: true
    },
    ...baseTablePropsList

  ]
}
export { tableConfig }
