export const rules = {
  account: [
    { required: true, message: '请输入账号', trigger: 'blur' },
    { min: 2, message: '3位有效字符', trigger: 'blur' },
  ],
  password: [
    { required: true, message: '请输入密码', trigger: 'blur' },
    { pattern: /^[a-zA-Z0-9]{3,9}$/, message: '4-9位数字或字母', trigger: 'blur' },
  ],
}

