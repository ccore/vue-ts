import dayjs from 'dayjs'
import utc from "dayjs/plugin/utc"
dayjs.extend(utc)
const DATE_TIME_FORMAT = 'YYYY:MM:DD HH:mm:ss'
export const formDateUtcString = (utcString: string, format: string = DATE_TIME_FORMAT) => {
  return dayjs.utc(utcString).utcOffset(8).format(format)

}
export function formDateTimestampString(timestampString: string, format: string = DATE_TIME_FORMAT) {
  let timestamp = 0
  if (typeof timestampString === 'number') {
    timestampString = String(timestampString)
  }
  if (timestampString.length === 10) {


    timestamp = parseInt(timestampString) * 1000;
  } else {
    timestamp = parseInt(timestampString);
  }
  return dayjs(timestamp).format(format)

}
