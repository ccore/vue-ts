import { createStore, Store, useStore as vuexStore } from 'vuex'
import defaultConfig from './defaultConfig/defaultConfig'
import login from './login/login'
import user from "./main/system/user/user"
import role from "./main/system/role/role"
import menu from "./main/system/menu/menu"
import dashboard from "./main/analysis/dashboard/dashboard"
import overview from "./main/analysis/overView/overview"
import category from "./main/system/category/category"
import article from "./main/system/article/article"
import comment from "./main/system/comment/comment"
import { IRootState, IStoreType } from "./types"
import { getRoleList } from "@/servers/main/system/role/role"
import { getTreeMenuList } from "@/servers/main/system/menu/menu"
import { getUserList } from "@/servers/main/system/user/user"
import { getCategoryList } from "@/servers/main/system/category/category"
import { getArticleList } from "@/servers/main/system/article/article"



const store = createStore<IRootState>({
  state: () => {
    return {
      queryInfo: {},
      entireRoles: [],
      entireMenus: [],
      entireUsers: [],
      entireCategorys: [],
      entireArticles: [],
    }
  },
  mutations: {
    saveQueryInfo(state, queryInfo) {
      state.queryInfo = queryInfo
    },
    changeEntireRoles(state, entireRoles) {
      state.entireRoles = entireRoles
    },
    changEntireMenus(state, entireMenus) {
      state.entireMenus = entireMenus
    },
    changEntireUsers(state, entireUsers) {
      state.entireUsers = entireUsers
    },
    changEntireCategorys(state, entireCategorys) {
      state.entireCategorys = entireCategorys
    },
    changEntireArticles(state, entireArticles) {
      state.entireArticles = entireArticles
    },
    rest_statue: (state) => {
      state.queryInfo = {}
      state.entireRoles = []
      state.entireMenus = []
      state.entireUsers = []
      state.entireCategorys = []
      state.entireArticles = []
    },

  },
  actions: {
    async getEntireRoles({ commit }) {
      const { code, data: { rows } } = await getRoleList({
        queryInfo: {
          otherWhere: {
            status: true
          }
        }
      })
      if (code === 1) {
        commit('changeEntireRoles', rows)
      }
    },
    async getEntireMenus({ commit }) {
      const { code, data: { rows } } = await getTreeMenuList()
      if (code == 1) {
        commit('changEntireMenus', rows)
      }
    },
    async getEntireUsers({ commit }) {
      const { code, data: { rows } } = await getUserList()
      if (code == 1) {
        commit('changEntireUsers', rows)
      }
    },
    async getEntireCategorys({ commit }) {
      const { code, data: { rows } } = await getCategoryList()
      if (code == 1) {
        commit('changEntireCategorys', rows)
      }
    },
    async getEntireArticles({ commit }) {
      const { code, data: { rows } } = await getArticleList()
      if (code == 1) {
        commit('changEntireArticles', rows)
      }
    },
    clearAll({ commit }) {
      let cache = ['user', 'role', 'menu', 'category', 'defaultConfig', 'overview', 'dashboard', 'article']
      cache.forEach(item => { commit(`${item}/reset`) })
      // console.log(1);
      commit('login/loginOut')
    }
  },
  modules: {
    login,
    user,
    menu,
    role,
    dashboard,
    overview,
    defaultConfig,
    category,
    article,
    comment
  }
})
export function setupStore() {
  store.dispatch('login/loadStore')
  store.dispatch('defaultConfig/loadStore')
  store.dispatch('dashboard/TotalAction')
  store.dispatch('overview/userByRoleAction')
  store.dispatch('overview/userGroupByGenderAction')
}
// 为 Store 类型扩充
export function useStore(): Store<IStoreType> {
  return vuexStore()
}
export default store
