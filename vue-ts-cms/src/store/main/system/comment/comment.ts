import { Module } from "vuex";
import type { IRootState } from "@/store/types"
import type { ICommentState } from "@/store/main/system/comment/types"
import type { DataType, IQueryInfo, IDelAllInfo } from "@/servers/types"
import { ElMessage } from "element-plus";
import { getCommentList, delCommentAll, createOrEditComment, createdBatchComment } from "@/servers/main/system/comment/comment"
const CommentStore: Module<ICommentState, IRootState> = {
  namespaced: true,
  state() {
    return {
      commentList: { rows: [], count: 0 }
    }
  },
  mutations: {
    changeCommentList(state, commentList) {
      state.
        commentList = commentList
    },
    reset(state) {
      state.commentList = { rows: [], count: 0 }
    }

  },
  actions: {
    async commentListAction({ commit }, queryInfo?: IQueryInfo) {
      const { code, data } = await getCommentList(queryInfo)
      if (code === 1) {
        commit('changeCommentList', data)
      }
    },
    async commentDelAllAction({ commit, dispatch }, { ids }: IDelAllInfo) {
      const { code, message } = await delCommentAll(ids)
      if (code == 1) {
        ElMessage.success(message)
        await dispatch('commentListAction', {
          queryInfo: this.state.queryInfo
        })
      }
    },
    async commentCreateOrEditAction({ commit, dispatch }, payload: any) {
      const { code, message } = await createOrEditComment(payload)
      if (code === 1) {
        ElMessage.success(message)
        await dispatch('commentListAction', {
          queryInfo: this.state.queryInfo
        })
      }
    },
    async commentCreateBatchAction({ commit, dispatch }, payload: any) {
      const { code, message } = await createdBatchComment(payload)
      if (code === 1) {
        ElMessage.success(message)
      }
    },

  },
}


export default CommentStore
