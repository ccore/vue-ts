import { Module } from "vuex";
import type { IRootState } from "@/store/types"
import type { IUserState } from "@/store/main/system/user/types"
import type { IQueryInfo, IDelAllInfo } from "@/servers/types"
import { getUserList, delUserAll, createOrEditUser, createdBatchUser, resetPassword, updateAvatar } from "@/servers/main/system/user/user"
import { ElMessage } from "element-plus";
const UserStore: Module<IUserState, IRootState> = {
  namespaced: true,
  state() {
    return {
      userList: { rows: [], count: 0 },
    }
  },
  mutations: {
    changeUserList(state, userList) {
      state.userList = userList
    },
    reset(state) {
      state.userList = { rows: [], count: 0 }
    }
  },
  actions: {
    async userListAction({ commit }, queryInfo?: IQueryInfo) {


      const { code, data } = await getUserList(queryInfo)
      if (code === 1) {
        commit('changeUserList', data)
      }
    },
    async userDelAllAction({ commit, dispatch }, { ids }: IDelAllInfo) {
      const { code, message } = await delUserAll(ids)
      if (code) {
        ElMessage.success(message)
        await dispatch('userListAction', {
          queryInfo: this.state.queryInfo
        })
      }
    },
    async userCreateOrEditAction({ commit, dispatch }, payload: any) {
      const { code, message } = await createOrEditUser(payload)
      if (code === 1) {
        ElMessage.success(message)
        await dispatch('userListAction', {
          queryInfo: this.state.queryInfo
        })
      }
    },

    async userCreateBatchAction({ commit, dispatch }, payload: any) {
      const { code, message } = await createdBatchUser(payload)
      if (code === 1) {
        ElMessage.success(message)
      }
    },
    async resetPasswordAction({ commit, dispatch }, payload: any) {
      const { code, message } = await resetPassword(payload)
      if (code === 1) {
        ElMessage.success(message)
      }
    },
    async updateAvatarAction({ commit, dispatch }, info: any) {
      const { code, message } = await updateAvatar(info.id, info.formData, info.store)
      if (code === 1) {
        ElMessage.success(message)
      }
    },

  },
}
export default UserStore
