import { Module } from "vuex";
import type { IRootState } from "@/store/types"
import type { ICategoryState } from "@/store/main/system/category/types"
import type { DataType, IQueryInfo, IDelAllInfo } from "@/servers/types"
import { ElMessage } from "element-plus";
import { getCategoryList, delCategoryAll, createOrEditCategory, createdBatchCategory } from "@/servers/main/system/category/category"
const CategoryStore: Module<ICategoryState, IRootState> = {
  namespaced: true,
  state() {
    return {
      categoryList: { rows: [], count: 0 }
    }
  },
  mutations: {
    changeCategoryList(state, categoryList) {
      state.categoryList = categoryList
      // LStorage.set(USER_LIST,categoryList)
    },
    reset(state) {
      state.categoryList = { rows: [], count: 0 }
    }
  },
  actions: {
    async categoryListAction({ commit }, queryInfo?: IQueryInfo) {
      const { code, data } = await getCategoryList(queryInfo)
      if (code === 1) {
        commit('changeCategoryList', data)
      }
    },
    async categoryDelAllAction({ commit, dispatch }, { ids }: IDelAllInfo) {
      const { code, message } = await delCategoryAll(ids)
      if (code == 1) {
        ElMessage.success(message)
        await dispatch('categoryListAction', {
          queryInfo: this.state.queryInfo
        })
      }
    },
    async categoryCreateOrEditAction({ commit, dispatch }, payload: any) {
      const { code, message } = await createOrEditCategory(payload)
      if (code === 1) {
        ElMessage.success(message)
        await dispatch('categoryListAction', {
          queryInfo: this.state.queryInfo
        })
      }
    },
    async categoryCreateBatchAction({ commit, dispatch }, payload: any) {
      const { code, message } = await createdBatchCategory(payload)
      if (code === 1) {
        ElMessage.success(message)
      }
    },
  },
}


export default CategoryStore
