interface ICategoryItemState {
  count: number
  rows: any[]

}
export interface ICategoryState {
  categoryList: ICategoryItemState
}
