import { Module } from "vuex";
import type { IRootState } from "@/store/types"
import type { IRoleState } from "@/store/main/system/role/types"
import type { DataType, IQueryInfo, IDelAllInfo } from "@/servers/types"
import { ElMessage } from "element-plus";
import { getRoleList, delRoleAll, createOrEditRole, createdBatchRole } from "@/servers/main/system/role/role"
const RoleStore: Module<IRoleState, IRootState> = {
  namespaced: true,
  state() {
    return {
      roleList: { rows: [], count: 0 }
    }
  },
  mutations: {
    changeRoleList(state, roleList) {
      state.roleList = roleList
      // LStorage.set(USER_LIST,roleList)
    },
    reset(state) {
      state.roleList = { rows: [], count: 0 }
    }

  },
  actions: {
    async roleListAction({ commit }, queryInfo?: IQueryInfo) {
      const { code, data } = await getRoleList(queryInfo)
      if (code === 1) {
        commit('changeRoleList', data)
      }
    },
    async roleDelAllAction({ commit, dispatch }, { ids }: IDelAllInfo) {
      const { code, message } = await delRoleAll(ids)
      if (code == 1) {
        ElMessage.success(message)
        await dispatch('roleListAction', {
          queryInfo: this.state.queryInfo
        })
      }
    },
    async roleCreateOrEditAction({ commit, dispatch }, payload: any) {
      const { code, message } = await createOrEditRole(payload)
      if (code === 1) {
        ElMessage.success(message)
        await dispatch('roleListAction', {
          queryInfo: this.state.queryInfo
        })
      }
    },
    async roleCreateBatchAction({ commit, dispatch }, payload: any) {
      const { code, message } = await createdBatchRole(payload)
      if (code === 1) {
        ElMessage.success(message)
      }
    },
  },
}


export default RoleStore
