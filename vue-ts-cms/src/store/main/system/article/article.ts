import { Module } from "vuex";
import type { IRootState } from "@/store/types"
import type { IArticleState } from "@/store/main/system/article/types"
import type { DataType, IQueryInfo, IDelAllInfo } from "@/servers/types"
import { ElMessage } from "element-plus";
import { getArticleList, delArticleAll, createOrEditArticle, createdBatchArticle } from "@/servers/main/system/article/article"
const ArticleStore: Module<IArticleState, IRootState> = {
  namespaced: true,
  state() {
    return {
      articleList: { rows: [], count: 0 }
    }
  },
  mutations: {
    changeArticleList(state, articleList) {
      state.articleList = articleList
      // LStorage.set(USER_LIST,articleList)
    },
    reset(state) {
      state.articleList = { rows: [], count: 0 }
    }
  },
  actions: {
    async articleListAction({ commit }, queryInfo?: IQueryInfo) {
      const { code, data } = await getArticleList(queryInfo)
      if (code === 1) {
        commit('changeArticleList', data)
      }
    },
    async articleDelAllAction({ commit, dispatch }, { ids }: IDelAllInfo) {
      const { code, message } = await delArticleAll(ids)
      if (code == 1) {
        ElMessage.success(message)
        await dispatch('articleListAction', {
          queryInfo: this.state.queryInfo
        })
      }
    },
    async articleCreateOrEditAction({ commit, dispatch }, payload: any) {
      const { code, message } = await createOrEditArticle(payload)
      if (code === 1) {
        ElMessage.success(message)
        await dispatch('articleListAction', {
          queryInfo: this.state.queryInfo
        })
      }
    },
    async articleCreateBatchAction({ commit, dispatch }, payload: any) {
      const { code, message } = await createdBatchArticle(payload)
      if (code === 1) {
        ElMessage.success(message)
      }
    },
  },
}


export default ArticleStore
