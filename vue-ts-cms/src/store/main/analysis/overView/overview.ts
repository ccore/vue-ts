import { Module } from "vuex"
import { IRootState, IOverViewSate } from "@/store/types"
import { getUserByRole, getUserGroupByGender } from "@/servers/main/analysis/overview/overview"
const overViewStore: Module<IOverViewSate, IRootState> = {
  namespaced: true,
  state() {
    return {
      userByRole: [],
      userGroupByGender: []
    }
  },
  mutations: {
    changeUserByRole(state, userByRole) {
      state.userByRole = userByRole
    },
    changeUserGroupByGender(state, userGroupByGender) {
      state.userGroupByGender = userGroupByGender
    },
    reset(state) {
      state.userByRole = []
      state.userGroupByGender = []
    }

  },
  actions: {
    async userByRoleAction({ commit, dispatch }) {

      const { code, data } = await getUserByRole()
      if (code == 1) {
        commit("changeUserByRole", data)
      }

    },
    async userGroupByGenderAction({ commit, dispatch }) {
      const { code, data } = await getUserGroupByGender()
      if (code == 1) {
        commit("changeUserGroupByGender", data)
      }
    }

  }
}
export default overViewStore
