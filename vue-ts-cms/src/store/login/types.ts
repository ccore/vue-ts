
// 登录类型接口


interface ILoginState {
  token: string
  userInfo: any
  menus: any
  permission: Map<string, string>,
  verifyCode: string
}
export {
  ILoginState,
}

