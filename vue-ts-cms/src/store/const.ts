export const TOKEN = 'token'
export const USER_INFO = 'userInfo'
export const CACHE_LOGIN_USER_INFO = 'loginUserInfo'
export const MENU = 'menus'
export const IS_FOLD = 'isFold' // 是否折叠
export const USER_LIST = 'userList'
export const ENTIRE_ROLES = 'entireRoles'

