import { App } from 'vue'
import { components, plugins } from './element/index'

// 按需导入Element Plus组件和插件
export default (app: App) => {
  components.forEach(component => {
    app.component(component.name, component)
  })
  plugins.forEach(plugin => {
    app.use(plugin)
  })


}
