let BAST_URL = ''
const TIME_OUT = 5000
if (process.env.NODE_ENV === 'development') {
  // 开发环境
  BAST_URL = '/api'
} else if (process.env.NODE_ENV === 'production') {
  // 生产环境
  BAST_URL = 'http://xxx.xx.xxx.xxx:xxxx/system'
}

export { BAST_URL, TIME_OUT }
