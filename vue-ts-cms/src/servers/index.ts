import MyRequest from './request/index'
import { BAST_URL, TIME_OUT } from './request/config'
import { LStorage } from "@/utils/cacheInfo"
import { TOKEN } from "@/store/const"
const myRequest = new MyRequest({
  baseURL: BAST_URL,
  timeout: TIME_OUT,
  intercepts: {
    requestHook: (config) => {
      const token = LStorage.get(TOKEN)?.token
      if (config.headers) {
        if (token) {
          config.headers.Authorization = `Bearer ${token}`
        }
      }
      return config
    }
  }
})

export default myRequest
