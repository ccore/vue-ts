import myRequest from "@/servers/index"
import { DataType } from "@/servers/types"
enum DashboardAPI {
  total = "/analysis/total"
}
export const getDashboardList = () => {
  return myRequest.get<DataType>({
    url: DashboardAPI.total
  })
}
