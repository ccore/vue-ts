import myRequest from "@/servers/index"
import { DataType, IQueryInfo } from "@/servers/types"
enum CategoryAPI {
  categoryList = "/categorys/categoryList",
  categoryDelAll = '/categorys/delAll',
  categoryCreate = '/categorys',
  categoryEdit = '/categorys/',
  createdBatch = '/categorys/bulkCreate'
}
export const getCategoryList = (queryInfo?: IQueryInfo) => {
  return myRequest.post<DataType>({
    url: CategoryAPI.categoryList,
    data: queryInfo,
  })
}


export const delCategoryAll = (ids: number[]) => {
  return myRequest.delete<DataType>({
    url: CategoryAPI.categoryDelAll,
    data: { ids }
  })
}

export const createOrEditCategory = (payload: any) => {
  if (payload.id) {
    delete payload.password
    return myRequest.put<DataType>({
      url: CategoryAPI.categoryEdit + payload.id,
      data: payload
    })
  } else {
    return myRequest.post<DataType>({
      url: CategoryAPI.categoryCreate,
      data: payload
    })
  }
}
export const createdBatchCategory = (arr: any[]) => {
  return myRequest.post<DataType>({
    url: CategoryAPI.createdBatch,
    data: arr
  })
}
