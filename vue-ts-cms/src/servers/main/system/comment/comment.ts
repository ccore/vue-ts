import myRequest from "@/servers/index"
import { DataType, IQueryInfo } from "@/servers/types"
enum CommentAPI {
  commentList = "/comments/commentList",
  commentDelAll = '/comments/delAll',
  commentCreate = '/comments',
  commentEdit = '/comments/',
  createdBatch = '/comments/bulkCreate'
}
export const getCommentList = (queryInfo?: IQueryInfo) => {
  return myRequest.post<DataType>({
    url: CommentAPI.commentList,
    data: queryInfo,
  })
}


export const delCommentAll = (ids: number[]) => {
  return myRequest.delete<DataType>({
    url: CommentAPI.commentDelAll,
    data: { ids }
  })
}

export const createOrEditComment = (payload: any) => {
  if (payload.id) {
    delete payload.password
    return myRequest.put<DataType>({
      url: CommentAPI.commentEdit + payload.id,
      data: payload
    })
  } else {
    return myRequest.post<DataType>({
      url: CommentAPI.commentCreate,
      data: payload
    })
  }
}
export const createdBatchComment = (arr: any[]) => {
  return myRequest.post<DataType>({
    url: CommentAPI.createdBatch,
    data: arr
  })
}
