import myRequest from "@/servers/index"
import { DataType, IQueryInfo } from "@/servers/types"
enum ArticleAPI {
  articleList = "/articles/articleList",
  articleDelAll = '/articles/delAll',
  articleCreate = '/articles',
  articleEdit = '/articles/',
  createdBatch = '/articles/bulkCreate'
}
export const getArticleList = (queryInfo?: IQueryInfo) => {
  return myRequest.post<DataType>({
    url: ArticleAPI.articleList,
    data: queryInfo,
  })
}


export const delArticleAll = (ids: number[]) => {
  return myRequest.delete<DataType>({
    url: ArticleAPI.articleDelAll,
    data: { ids }
  })
}

export const createOrEditArticle = (payload: any) => {
  if (payload.id) {
    delete payload.password
    return myRequest.put<DataType>({
      url: ArticleAPI.articleEdit + payload.id,
      data: payload
    })
  } else {
    return myRequest.post<DataType>({
      url: ArticleAPI.articleCreate,
      data: payload
    })
  }
}
export const createdBatchArticle = (arr: any[]) => {
  return myRequest.post<DataType>({
    url: ArticleAPI.createdBatch,
    data: arr
  })
}
