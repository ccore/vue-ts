interface IDropdownItemType {
  text: string
  url?: string
  icon?: string
  isLine?: boolean
}


export {
  IDropdownItemType
}
