import type { IDropdownItemType } from "../types.ts"
export const dropdowMenuItems: IDropdownItemType[] = [
  // {
  //   icon: 'edit',
  //   text: "个人中心",
  //   url: "/main/system/userInfo",

  // },
  // {
  //   icon: 'edit',
  //   text: "修改密码",
  //   // url: "/main/system/userInfo",

  // },
  {
    icon: 'color',
    text: "切换主题",

  },
  {
    icon: 'loginOut',
    text: "退出登录",
    url: "/loginOut",
    isLine: true
  }
]
export * from "../types.ts"
export default dropdowMenuItems
