// interface IPropsList {
//   label?: string
//   prop?: string,
//   slotName: string
//   minWidth?: string
//   fixed?: string,
// }

interface IPagination {
  currentPage: number
  pageSize: number,
  total?: number
  layout?: string
  background?: string
  small?: string
  pageSizes?: Array<number>

}


interface ITableConfig {
  title: string
  showIndex?: boolean,
  showSelection?: boolean
  showPaging?: boolean,
  propsList: any[]
  childrenProps?: any
  otherOptions?: any

}
export { ITableConfig, IPagination }
