type IFormItemType = 'input' | 'password' | 'select' | 'datepicker' | 'checkbox' | 'radio' | 'textarea' | 'autocomplete' | 'prependInput'
interface IFormItem {

  field: string
  label?: string
  rule?: any[]
  type: IFormItemType
  placeholder?: string
  options?: any[]
  otherOptions?: any
  isHidden?: boolean
  prepend?: string // 输入框前置插槽内容
  defaultValue?: any// 默认值
  isAssociate?: boolean,// 是否存在关联
  associateName?: string // 关联数据名称
  associateKey?: string // 关联数据的关联字段


}
interface IFormConfig {
  labelWidth?: string
  labelPosition?: string
  itemStyle?: object
  colLayout?: object
  formItems: IFormItem[]
}

export { IFormConfig, IFormItem }
