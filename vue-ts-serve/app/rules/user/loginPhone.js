'use strict'

const rule = {
	phone: [
		{ required: true, message: '手机号不能为空' },
		{
			min: 11,
			max: 11,
			message: '手机号长度为11为数字',
		},
	],
	verifyCode: [
		{ required: true, message: '短信验证码不能为空' },
		{
			pattern: /^[a-zA-Z0-9]{4,9}$/,
			message: '密码格式为6位数字或字母',
		},
	],
}

module.exports = rule
