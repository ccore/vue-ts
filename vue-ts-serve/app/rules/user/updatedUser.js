'use strict'

const rule = {
	id: [{ required: true, message: 'id不能为空' }],
	username: [
		{ message: '账号不能为空' },
		{
			min: 2,
			message: '账号格式为至少2位有效字符',
		},
	],
	password: [
		{ message: '密码不能为空' },
		{
			pattern: /^[a-zA-Z0-9]{4,9}$/,
			message: '密码格式为4-9位数字或字母',
		},
	],
	gender: [
		{
			type: 'enum',
			enum: [0, 1, 2],
			message: '性别为:0,1,2的其中一个,0保密,1男，2女',
		},
	],
}

module.exports = rule
