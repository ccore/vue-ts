'use strict'

const rule = {
	name: [
		{ required: true, message: '分类名称不能为空' },
		{
			min: 2,
			max: 15,
			message: '分类名称2-15位有效字符',
		},
	],
	identification: [
		{ message: 'identification分类标识不能为空' },
		{
			min: 2,
			max: 20,
			message: 'identification分类标识格式2-20位有效字符',
		},
	],
	remark: [
		{ required: true, message: '评论内容不能为空' },
		{
			min: 1,
			max: 1000,
			message: '评论内容1-1000个有效字符范围内',
		},
	],
}

module.exports = rule
