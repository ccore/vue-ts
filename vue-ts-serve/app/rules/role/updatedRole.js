'use strict'

const rule = {
	id: [{ required: true, message: 'id不能为空' }],
	name: [
		{ message: '角色名称不能为空' },
		{
			min: 2,
			message: '角色格式为至少2位有效字符',
		},
	],
	identification: [
		{ message: 'identification标识不能为空' },
		{
			min: 2,
			max: 20,
			message: 'identification角色标识格式2-20位有效字符',
		},
	],
	description: [
		{ message: '描述不能为空' },
		{
			min: 2,
			max: 60,
			message: '角色描述2-60个字符',
		},
	],
}

module.exports = rule
