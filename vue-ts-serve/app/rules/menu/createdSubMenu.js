'use strict'

const rule = {
	menu_name: [
		{ required: true, message: '菜单名称不能为空' },
		{
			min: 2,
			message: '菜单格式为至少2位有效字符',
		},
	],
	parent_pid: [{ required: true, message: '父id不能为空' }],
	remark: [
		{ message: '描述不能为空' },
		{
			min: 2,
			max: 60,
			message: '菜单描述2-60个字符',
		},
	],
	path: [
		{ required: true, message: '菜单路径不能为空' },
		{
			// eslint-disable-next-line no-unused-vars
			validator(rule, value, callback, source, options) {
				const pattern = /[\u4E00-\u9FA5]+/
				if (pattern.test(value)) {
					callback({ message: '路径中不能包含中文' }) // 验证不通过
					return
				}
				callback() // 验证通过
			},
		},
	],
}

module.exports = rule
