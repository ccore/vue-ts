'use strict'
const rule = {
	menu_name: [
		{ required: true, message: '菜单名称不能为空' },
		{
			min: 2,
			message: '菜单格式为至少2位有效字符',
		},
	],
	icon: [{ required: true, message: '菜单图标不能为空' }],
	parent_pid: [{ required: true, message: '父id不能为空' }],
	menu_type: [{ required: true, message: '类型标识不能为空' }],
	remark: [
		{ message: '描述不能为空' },
		{
			min: 2,
			max: 60,
			message: '菜单描述2-60个字符',
		},
	],
}

module.exports = rule
