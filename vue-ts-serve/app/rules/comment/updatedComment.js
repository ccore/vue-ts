'use strict'

const rule = {
	content: [
		{ required: true, message: '评论内容不能为空' },
		{
			min: 1,
			max: 1000,
			message: '评论内容1-1000个有效字符范围内',
		},
	],
}

module.exports = rule
