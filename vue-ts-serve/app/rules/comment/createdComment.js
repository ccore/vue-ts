'use strict'

const rule = {
	form_uid: [{ required: true, message: '发表评论的用户id不能为空' }],
	content: [
		{ required: true, message: '评论内容不能为空' },
		{
			min: 1,
			max: 1000,
			message: '评论内容1-1000个有效字符范围内',
		},
	],
	article_id: [{ required: true, message: '被评论文章不能为空' }],
}

module.exports = rule
