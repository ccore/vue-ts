'use strict'
module.exports = (app) => {
	const { STRING, INTEGER, DATE } = app.Sequelize

	const CategoryArticle = app.model.define(
		't_cate_article',
		{
			cate_id: INTEGER,
			article_id: INTEGER,
		},
		{
			tableName: 't_cate_article', //指定表名称
			timestamps: false,
		}
	)
	return CategoryArticle
}
