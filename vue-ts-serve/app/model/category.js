'use strict'

module.exports = (app) => {
	const { STRING, INTEGER, DATE, BOOLEAN } = app.Sequelize
	const Category = app.model.define(
		't_category',
		{
			id: { type: INTEGER, primaryKey: true, autoIncrement: true },
			name: STRING(30),
			identification: STRING,
			status: {
				type: STRING,
				default: 1,
			},
			remark: STRING,
			createdAt: DATE,
			updatedAt: DATE,
			deletedAt: DATE,
		},
		{
			tableName: 't_category', //指定表名称
			timestamps: true,
		}
	)
	Category.associate = function () {
		Category.belongsToMany(app.model.Article, {
			through: app.model.CategoryArticle,
			foreignKey: 'cate_id',
			otherKey: 'article_id',
			as: 'categories',
		})
	}

	return Category
}
