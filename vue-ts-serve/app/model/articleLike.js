'use strict'
module.exports = (app) => {
	const { STRING, INTEGER, BOOLEAN, DATE } = app.Sequelize

	const ArticleLike = app.model.define(
		't_article_like',
		{
			id: { type: INTEGER, primaryKey: true, autoIncrement: true },
			user_id: INTEGER,
			article_id: INTEGER,
		},
		{
			timestamps: false,
			tableName: 't_article_like',
		}
	)
	ArticleLike.associate = function () {
		ArticleLike.hasOne(app.model.User, {
			foreignKey: 'id',
			sourceKey: 'user_id',
			as: 'likeUser',
		})
		ArticleLike.hasOne(app.model.Article, {
			foreignKey: 'id',
			sourceKey: 'article_id',
			as: 'articleDetail',
		})
	}
	return ArticleLike
}
