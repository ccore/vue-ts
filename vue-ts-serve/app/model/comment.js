'use strict'
module.exports = (app) => {
	const { STRING, INTEGER, BOOLEAN, DATE } = app.Sequelize

	const Comment = app.model.define(
		't_comment',
		{
			id: {
				type: INTEGER,
				primaryKey: true,
				allowNull: true,
			},
			form_uid: INTEGER,
			content: STRING,
			article_id: INTEGER,
			createdAt: DATE,
			updatedAt: DATE,
			deletedAt: DATE,
		},
		{
			timestamps: true,
			tableName: 't_comment',
		}
	)
	// 一对多 一条评论可以有多个回复
	Comment.associate = function () {
		Comment.hasMany(app.model.Reply, {
			foreignKey: 'comm_id',
			targetKey: 'id',
			as: 'replies',
		})
		Comment.hasOne(app.model.Article, {
			foreignKey: 'id',
			targetKey: 'article_id',
			as: 'articleDetail',
		})
		Comment.hasOne(app.model.User, {
			foreignKey: 'id',
			sourceKey: 'form_uid',
			as: 'userDetail',
		})
	}
	return Comment
}
