'use strict'
module.exports = (app) => {
	const { STRING, INTEGER, BOOLEAN, DATE } = app.Sequelize

	const Reply = app.model.define(
		't_reply',
		{
			id: {
				type: INTEGER,
				primaryKey: true,
				allowNull: true,
			}, // 谁(from_uid)回复了谁(reply_target_id)， 回复的内容(content)是什么，针对哪条评论(comm_id)进行了回复
			comm_id: INTEGER, // 评论的id
			from_uid: INTEGER, //回复人的id
			reply_target_id: INTEGER, //回复目标的id
			content: STRING,
			createdAt: DATE,
			updatedAt: DATE,
			deletedAt: DATE,
		},
		{
			timestamps: true,
			tableName: 't_reply',
		}
	)
	Reply.associate = function () {
		Reply.hasOne(app.model.User, {
			foreignKey: 'id',
			sourceKey: 'from_uid',
			as: 'userDetail',
		})
	}
	return Reply
}
