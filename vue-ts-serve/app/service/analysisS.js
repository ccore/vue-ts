const Service = require('egg').Service

const { Op } = require('sequelize')

class AnalysisService extends Service {
	async total() {
		const { model } = this.ctx
		const menuTotal = await model.Menu.count({ where: { status: 1 } })
		const userTotal = await model.User.count({ where: { status: true } })
		const roleTotal = await model.Role.count({ where: { status: true } })
		const categoryTotal = await model.Category.count({
			where: { status: true },
		})
		const articleTotal = await model.Article.count({
			where: { status: true },
		})
		const commentTotal = await model.Comment.count()
		return {
			flag: true,
			data: [
				{
					title: '用户数量',
					count: userTotal,
				},
				{
					title: '角色数量',
					count: roleTotal,
				},
				{
					title: '菜单数量',
					count: menuTotal,
				},
				{
					title: '分类标签',
					count: categoryTotal,
				},
				{
					title: '文章',
					count: articleTotal,
				},
				{
					title: '评论',
					count: commentTotal,
				},
			],
		}
	}
	async userByRole() {
		const { model } = this.ctx
		return await model.Role.findAll({
			attributes: ['name'],
			include: [
				{
					attributes: ['username'],
					model: this.ctx.model.User,
					as: 'userRoles',
					through: {
						attributes: [],
					},
					where: {
						status: true,
					},
				},
			],
			where: {
				status: true,
			},
		})
	}
	async userGroupByGender() {
		const { model } = this.ctx
		return await model.User.count({
			attributes: ['gender'],
			where: {
				status: true,
			},
			group: 'gender',
		})
	}
}

module.exports = AnalysisService
