const BaseService = require('./baseService')
const { Op } = require('sequelize')
class CategoryService extends BaseService {
	constructor(...args) {
		super(...args)
		this.modelName = 'Category'
	}
}

module.exports = CategoryService
