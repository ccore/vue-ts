const BaseService = require('./baseService')
const { Op } = require('sequelize')
class LoginService extends BaseService {
	constructor(...args) {
		super(...args)
		this.modelName = 'User'
	}
	async loginAccount(options) {
		return await this.ctx.model[this.modelName].findOne({
			attributes: {
				exclude: ['createdAt', 'updatedAt', 'deletedAt'],
			},
			where: {
				username: {
					[Op.eq]: options.username,
				},
			},
			include: [
				{
					model: this.ctx.model.Role,
					as: 'userRoles',
					attributes: {
						exclude: ['createdAt', 'updatedAt', 'deletedAt', 'status'],
					},
					through: {
						attributes: [],
					},
				},
			],
		})
	}
}

module.exports = LoginService
