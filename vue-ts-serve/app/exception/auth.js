// app/exception/auth.js
const HttpException = require('./http')
class AuthException extends HttpException {
	constructor(message = '令牌无效', errorCode = 401) {
		super(errorCode, message, null, 401)
	}
}
module.exports = AuthException
