'use strict'
const BaseController = require('./baseController')

class ArticleController extends BaseController {
	constructor(...args) {
		super(...args)
		this.serviceName = 'articleS'
		this.uniqueWords = ['title']
		this.validateUpdatePath = 'article.updatedArticle' // 对应的修改的参数校验规则
		this.validateCreatedPath = 'article.createdArticle' //对应的创建的参数校验规则
		this.moduleObj = {
			//删除角色 同时删除和角色关联的菜单
			// 查询角色同时查询关联菜单
			isDelAssociate: true, // 如果数据存在关联 删除时是否能一起删除
			associateModelName: 'CategoryArticle',
			associateModelForeignKey: 'article_id',
			associateModelOtherKey: 'cate_id',
			associateModelAs: 'categories',
			associateModelOtherModel: 'Category', // 附表的模型名称
			associateModelConfig: [true, true, true, true], // 关联模型的配置   增 删 改 查  true/是  false/否
		}
	}
}
module.exports = ArticleController
