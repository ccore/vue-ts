'use strict'
const BaseController = require('./baseController')

class CategoryController extends BaseController {
	constructor(...args) {
		super(...args)
		this.serviceName = 'categoryS'
		this.uniqueWords = ['name']
		this.validateUpdatePath = 'category.updatedCategory' // 对应的修改的参数校验规则
		this.validateCreatedPath = 'category.createdCategory' //对应的创建的参数校验规则
		this.createdModuleObj = {
			//删除角色 同时删除和角色关联的菜单
			// 查询角色同时查询关联菜单
			isDelAssociate: false, // 如果数据存在关联 删除实是否能一起删除
			associateModelName: 'CategoryArticle',
			associateModelForeignKey: 'cate_id',
			associateModelOtherKey: 'article_id',
			associateModelAs: 'categories',
			associateModelOtherModel: 'Article', // 附表的模型名称
			associateModelConfig: [false, true, true, true], // 关联模型的配置   增 删 改 查  true/是  false/否
		}
	}
}
module.exports = CategoryController
