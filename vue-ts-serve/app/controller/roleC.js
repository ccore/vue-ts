'use strict'

const BaseController = require('./baseController')
class RoleController extends BaseController {
	constructor(...args) {
		super(...args)
		this.serviceName = 'roleS'
		this.uniqueWords = ['name', 'identification']
		this.validateUpdatePath = 'role.updatedRole'
		this.validateCreatedPath = 'role.createdRole'

		this.createdModuleObj = {
			//删除角色 同时删除和角色关联的菜单
			// 查询角色同时查询关联菜单
			isDelAssociate: true, // 如果数据存在关联 删除时是否能一起删除
			associateModelName: 'RoleMenu',
			associateModelForeignKey: 'role_id',
			associateModelOtherKey: 'menu_id',
			associateModelAs: 'RoleMenu',
			associateModelOtherModel: 'Menu', // 附表的模型名称
			associateModelConfig: [false, true, true, true], // 关联模型的配置   增 删 改 查  true/是  false/否
		}
		this.moduleObj = {
			// 删除角色需要查询用户是否有关联的用户
			isDelAssociate: false, // 如果数据存在关联 删除实是否能一起删除
			associateModelName: 'RoleUser',
			associateModelForeignKey: 'role_id',
			associateModelOtherKey: 'user_id',
			associateModelAs: 'userRoles',
			associateModelOtherModel: 'Role', // 附表的模型名称
			associateModelConfig: [false, true, false, true], // 关联模型的配置   增 删 改 查  true/是  false/否
		}
	}
}

module.exports = RoleController
