const BaseMultiUploadFilesC = require('./baseMultiUploadFilesC')

class MultiUploadFileControl extends BaseMultiUploadFilesC {
	async createAvatar() {
		const { service } = this.ctx
		// 上传头像的,会在uploads文件夹下有个avatar的文件夹下面才是2019、06、21
		const { flag: uploadRes, data, params } = await this.MultiUploadFiles()
		const { id } = this.ctx.params
		if (uploadRes) {
			return this.success({ data: { files: data, params } })
			// 文件上传成功后才进行后续操作

			// let { flag, message } = await service.userS.update(id, {
			// 	avatar: `${this.app.config.baseUrl}${url}`,
			// })
			// if (flag) {
			// 	this.success({
			// 		data: `${this.app.config.baseUrl}${url}`,
			// 		message: '上传文件成功',
			// 	})
			// } else {
			// 	this.fail({ message })
			// }
		}
	}
}
module.exports = MultiUploadFileControl
